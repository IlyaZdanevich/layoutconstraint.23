//
//  MainView.swift
//  23.LayoutConstraint
//
//  Created by Harbros 70 on 4.08.21.
//

import UIKit

class MainView: UIView {

    // MARK: - Constans
    private enum Constans {
        static let messangeIdentifier = "messangeIdentifier"
    }

    // MARK: - Private Properte
    private let myTableView = UITableView()

    // MARK: - Public Properte
    var getDataFromModelHandler: () -> [MessageModel] = { return [MessageModel]() }

    // MARK: - Public Methods
    func createTableView() {
        addSubview(myTableView)
        backgroundColor = .white
        myTableView.separatorStyle = .none
        myTableView.dataSource = self
        myTableView.translatesAutoresizingMaskIntoConstraints = false

        myTableView.topAnchor.constraint(
            equalTo: topAnchor, constant: bounds.minY + UIApplication.shared.windows[.zero].safeAreaInsets.top
        ).isActive = true
        myTableView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        myTableView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        myTableView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true

        myTableView.register(MessageTableView.self, forCellReuseIdentifier: Constans.messangeIdentifier)
    }
}

// MARK: - UITableViewDataSource
extension MainView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        getDataFromModelHandler().count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constans.messangeIdentifier, for: indexPath)
                as? MessageTableView else { return UITableViewCell() }
        cell.selectionStyle = .none
        cell.setupCell(message: getDataFromModelHandler()[indexPath.row])
        return cell
    }
}
