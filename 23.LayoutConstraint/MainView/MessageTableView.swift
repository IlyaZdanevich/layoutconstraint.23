//
//  MessageTable.swift
//  23.LayoutConstraint
//
//  Created by Harbros 70 on 4.08.21.
//

import UIKit

class MessageTableView: UITableViewCell {

    // MARK: - Constans
    private enum Constans {
        static let sizeTextMessage: CGFloat = 17
        static let sizeTimeMessage: CGFloat = 10
        static let stileTextMessage = "Avenir-Light"
        static let stileTextTime = "Verdana"
        static let time = "23:46"

        static let shiftView: CGFloat = 20
        static let shiftTime: CGFloat = 25
        static let sweptArea: CGFloat = 1.4
        static let shiftLabel: CGFloat = 5
        static let shiftText: CGFloat = 30
        static let indentView: CGFloat = 2
        static let indentTime: CGFloat = 3
    }

    // MARK: - Private Properties
    private var rightPosition: Bool?
    private var text = UILabel()
    private var time = UILabel()
    private var messageView = MessageView()

    override func layoutSubviews() {
        super.layoutSubviews()
        messageView.createBackgroundSms(isFromClient: rightPosition ?? true)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        text.textColor = .black
        time.textColor = .black
        messageView.removeFromSuperview()
        text.removeFromSuperview()
        time.removeFromSuperview()
    }

    // MARK: - Public Methods
    func setupCell(message: MessageModel) {
        rightPosition = message.isFromClient
        time.text = "23:46"
        time.font = UIFont(name: Constans.stileTextTime, size: Constans.sizeTimeMessage)
        text.text = message.textLabel
        text.font = UIFont(name: Constans.stileTextMessage, size: Constans.sizeTextMessage)
        createConstraints()
        createTextLabel()
    }

    // MARK: - Private Methods
    private func createTextLabel() {
        text.numberOfLines = .zero
        guard !(rightPosition ?? true) else { return text.textColor = .white }
    }

    private func createConstraints() {
        addSubview(messageView)
        addSubview(text)
        addSubview(time)

        text.translatesAutoresizingMaskIntoConstraints = false
        messageView.translatesAutoresizingMaskIntoConstraints = false
        time.translatesAutoresizingMaskIntoConstraints = false

        if rightPosition ?? false {
            text.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constans.shiftText).isActive = true
            messageView.leadingAnchor.constraint(
                equalTo: text.leadingAnchor,
                constant: -Constans.shiftView)
                .isActive = true
            messageView.trailingAnchor.constraint(
                equalTo: text.trailingAnchor,
                constant: Constans.shiftLabel
            ).isActive = true
            time.leadingAnchor.constraint(equalTo: text.trailingAnchor, constant: -Constans.shiftText).isActive = true

        } else {
            text.trailingAnchor.constraint(
                equalTo: trailingAnchor,
                constant: -Constans.shiftText
            ).isActive = true
            messageView.leadingAnchor.constraint(
                equalTo: text.leadingAnchor,
                constant: -Constans.shiftLabel
            ).isActive = true
            messageView.trailingAnchor.constraint(
                equalTo: text.trailingAnchor,
                constant: Constans.shiftView
            ).isActive = true
            time.leadingAnchor.constraint(equalTo: text.trailingAnchor, constant: -Constans.shiftTime).isActive = true
        }
        time.topAnchor.constraint(equalTo: text.bottomAnchor, constant: -Constans.indentTime).isActive = true
        time.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Constans.shiftLabel).isActive = true

        text.topAnchor.constraint(equalTo: topAnchor, constant: Constans.shiftLabel).isActive = true
        text.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Constans.shiftView).isActive = true
        text.widthAnchor.constraint(
            lessThanOrEqualToConstant: UIScreen.main.bounds.width / Constans.sweptArea
        ).isActive = true
        messageView.topAnchor.constraint(equalTo: topAnchor, constant: Constans.indentView).isActive = true
       messageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Constans.indentView).isActive = true
    }
}
