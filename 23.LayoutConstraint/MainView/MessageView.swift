//
//  MessageView.swift
//  23.LayoutConstraint
//
//  Created by Harbros 70 on 4.08.21.
//

import UIKit

// MARK: - Constans
private enum Constants {
    static let inset: CGFloat = 5
    static let endMessage: CGFloat = 1
    static let mesEndCurve: CGFloat = 15
    static let curveControl: CGFloat = 8
    static let cornerRadius: CGFloat = 10
    static let endCorvePoint: CGFloat = 2
    static let messageCurve: CGFloat = 16
    static let firstMesPoint: CGFloat = 3
    static let endCurvePoint: CGFloat = 3
    static let leftCornerFix: CGFloat = 17
    static let startPointByX: CGFloat = 20
    static let endCurveMargin: CGFloat = 3
    static let bigHeightCurve: CGFloat = 16
    static let pointXAddition: CGFloat = 35
    static let firstStartPoint: CGFloat = 7
    static let firstMessEndPoint: CGFloat = 6
    static let messageHeightCurve: CGFloat = 4
}

// MARK: - MessageViewClass
class MessageView: UIView {

    // MARK: - Public Methods
    func createBackgroundSms(isFromClient: Bool) {
        let bezierPath = UIBezierPath()
        guard isFromClient else {
            createBackgroundSms(color: .systemBlue, path: bezierPath, frame: bounds)
            return
        }
        createSmsPoisoner(color: .separator, path: bezierPath, frame: bounds)
        bezierPath.close()
    }

    // MARK: - Private Methods
    private func createBackgroundSms(color: UIColor, path: UIBezierPath, frame: CGRect) {
        backgroundColor = color
        path.move(to: CGPoint(x: frame.width + Constants.startPointByX, y: frame.height))
        path.addLine(to: CGPoint(x: Constants.cornerRadius, y: frame.height))
        path.addCurve(
            to: CGPoint(x: .zero, y: frame.height - Constants.cornerRadius),
            controlPoint1: CGPoint(x: Constants.messageHeightCurve, y: frame.height),
            controlPoint2: CGPoint(x: .zero, y: frame.height - Constants.messageHeightCurve)
        )
        path.addLine(to: CGPoint(x: .zero, y: Constants.cornerRadius))
        path.addCurve(
            to: CGPoint(x: Constants.cornerRadius, y: .zero),
            controlPoint1: CGPoint(x: .zero, y: Constants.messageHeightCurve),
            controlPoint2: CGPoint(x: Constants.messageHeightCurve, y: .zero)
        )
        path.addLine(to: CGPoint(x: frame.width - Constants.startPointByX, y: .zero))
        path.addCurve(
            to: CGPoint(x: frame.width - Constants.cornerRadius, y: Constants.cornerRadius),
            controlPoint1: CGPoint(x: frame.width - Constants.bigHeightCurve, y: .zero),
            controlPoint2: CGPoint(x: frame.width - Constants.cornerRadius, y: Constants.messageHeightCurve)
        )
        path.addLine(to: CGPoint(x: frame.width - Constants.cornerRadius, y: frame.height - Constants.cornerRadius))
        path.addCurve(
            to: CGPoint(x: frame.width - Constants.messageHeightCurve, y: frame.height),
            controlPoint1: CGPoint(x: frame.width - Constants.curveControl, y: frame.height - Constants.inset),
            controlPoint2: CGPoint(
                x: frame.width - Constants.firstMessEndPoint,
                y: frame.height - Constants.firstMesPoint
            )
        )
        path.addCurve(
            to: CGPoint(x: frame.width - Constants.cornerRadius, y: frame.height - Constants.firstMesPoint),
            controlPoint1: CGPoint(x: frame.width - Constants.firstStartPoint, y: frame.height - Constants.endMessage),
            controlPoint2: CGPoint(x: frame.width - Constants.curveControl, y: frame.height - Constants.endCorvePoint))
        path.addCurve(
            to: CGPoint(
                x: frame.width - Constants.startPointByX,
                y: frame.height
            ),
            controlPoint1: CGPoint(
                x: frame.width - Constants.bigHeightCurve,
                y: frame.height - Constants.endCorvePoint
            ),
            controlPoint2: CGPoint(
                x: frame.width - Constants.mesEndCurve, y: frame.height - Constants.endMessage
            )
        )
        let layer = CAShapeLayer()
        layer.path = path.cgPath
        self.layer.mask = layer
    }

    private func createSmsPoisoner(color: UIColor, path: UIBezierPath, frame: CGRect) {
        backgroundColor = color
        path.move(to: CGPoint(x: Constants.startPointByX, y: frame.height))
        path.addLine(to: CGPoint(x: frame.width - Constants.cornerRadius, y: frame.height))
        path.addCurve(
            to: CGPoint(x: frame.width, y: frame.height - Constants.cornerRadius),
            controlPoint1: CGPoint(x: frame.width - Constants.messageHeightCurve, y: frame.height),
            controlPoint2: CGPoint(x: frame.width, y: frame.height - Constants.messageHeightCurve)
        )
        path.addLine(to: CGPoint(x: frame.width, y: Constants.cornerRadius))
        path.addCurve(
            to: CGPoint(x: frame.width - Constants.cornerRadius, y: .zero),
            controlPoint1: CGPoint(x: frame.width, y: Constants.messageHeightCurve),
            controlPoint2: CGPoint(x: frame.width - Constants.messageHeightCurve, y: .zero)
        )
        path.addLine(to: CGPoint(x: Constants.startPointByX, y: .zero))
        path.addCurve(
            to: CGPoint(x: Constants.cornerRadius, y: Constants.cornerRadius),
            controlPoint1: CGPoint(x: Constants.bigHeightCurve, y: .zero),
            controlPoint2: CGPoint(x: Constants.cornerRadius, y: Constants.messageHeightCurve)
        )
        path.addLine(to: CGPoint(x: Constants.cornerRadius, y: frame.height - Constants.cornerRadius))
        path.addCurve(
            to: CGPoint(x: Constants.messageHeightCurve, y: frame.height),
            controlPoint1: CGPoint(x: Constants.curveControl, y: frame.height - Constants.inset),
            controlPoint2: CGPoint(x: Constants.firstMessEndPoint, y: frame.height - Constants.firstMesPoint)
        )
        path.addCurve(
            to: CGPoint(x: Constants.messageCurve, y: frame.height - Constants.firstMesPoint),
            controlPoint1: CGPoint(
                x: Constants.firstStartPoint,
                y: frame.height - Constants.endMessage
            ),
            controlPoint2: CGPoint(
                x: Constants.curveControl,
                y: frame.height - Constants.endCorvePoint - Constants.endCurvePoint
            )
        )
        path.addCurve(
            to: CGPoint(x: Constants.pointXAddition, y: frame.height + Constants.endCurveMargin),
            controlPoint1: CGPoint(x: Constants.bigHeightCurve, y: frame.height - Constants.endCorvePoint),
            controlPoint2: CGPoint(x: Constants.leftCornerFix, y: frame.height - Constants.endMessage)
        )
        let layer = CAShapeLayer()
        layer.path = path.cgPath
        self.layer.mask = layer
    }
}
