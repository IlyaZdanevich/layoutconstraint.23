//
//  Message.swift
//  23.LayoutConstraint
//
//  Created by Harbros 70 on 4.08.21.
//

import UIKit

class Messages {

    // MARK: - Private Constans
    private enum Constans {
        static let maxCountGroup = 60
        static let minCountGroup = 40
        static let indexZero = 0
        static let minLetters = 5
        static let maxLetters = 150
        static let letters = "ABCbcdefghijklmnopqrstuvwxyz ,:"
        static var messagesArray = [String]()
    }

    // MARK: - Public Properties
    var messagesArray = [MessageModel]()

    // MARK: - Initializer
    init() {
        randomString()
        messagesArray  = Constans.messagesArray.map { _ in
            MessageModel(
                textLabel: Constans.messagesArray.randomElement() ?? String(),
                isFromClient: Bool.random()
            )
        }
    }

    // MARK: - Public Methods
    func countMessege() -> Int {
        let count = messagesArray.count
        return count
    }

    // MARK: Private Methods
    private func randomString() {
        for _ in Constans.indexZero..<Int.random(in: Constans.minCountGroup...Constans.maxCountGroup) {
            let randomString = (Constans.indexZero..<Int.random(in: Constans.minLetters...Constans.maxLetters))
                .map { _ in Constans.letters.randomElement() ?? String.Element("") }
            Constans.messagesArray += [String(randomString).capitalized]
        }
    }
}
