//
//  Messange.swift
//  23.LayoutConstraint
//
//  Created by Harbros 70 on 4.08.21.
//

import UIKit

struct MessageModel {
    var textLabel: String
    var isFromClient: Bool
}
