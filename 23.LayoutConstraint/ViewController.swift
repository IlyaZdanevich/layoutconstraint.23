//
//  ViewController.swift
//  23.LayoutConstraint
//
//  Created by Harbros 70 on 4.08.21.
//

import UIKit

class ViewController: UIViewController {

    // MARK: Private Propetes
    private let mainView = MainView()
    private let messangeModel = Messages()

    override func viewDidLoad() {
        super.viewDidLoad()
        view = mainView
        setHandlers()
        mainView.createTableView()
    }

    // MARK: - Private Methods
    private func setHandlers() {
        mainView.getDataFromModelHandler = { [weak self] in
            guard let massenges = self?.messangeModel.messagesArray else { return [MessageModel]()}
            return massenges
        }
    }
}
